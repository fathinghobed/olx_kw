<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="card">
					<div class="card-body">
						<div class="card-title">
							<h3 class="text-center title-2">Tambah Kategori</h3>
						</div>
						<hr>
						<form action="" method="post" novalidate="novalidate">
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">Sub Kategori dari</label>
								<input id="parent_id" name="cc-name" type="text" class="form-control cc-name valid" data-val="true" data-val-required="Masukan Kategori" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error" placeholder="Kosongkan jika ingin membuat Kategori baru ....">
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">Nama Kategori</label>
								<input id="name" name="cc-nama" type="text" class="form-control cc-nama valid" data-val="true" data-val-required="Masukan Nama Barang" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
						</form>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button id="tambahkan" type="button" class="btn btn-primary">Tambah</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="card">
					<div class="card-body">
						<div class="card-title">
							<h3 class="text-center title-2">Update Kategori</h3>
						</div>
						<hr>
						<form action="" method="post" novalidate="novalidate">
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">ID</label>
								<input id="update_id" name="cc-name" type="text" class="form-control cc-name valid" data-val="true" data-val-required="Masukan Kategori" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error" readonly="">
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">Sub Kategori dari</label>
								<input id="update_parent_id" name="cc-name" type="text" class="form-control cc-name valid" data-val="true" data-val-required="Masukan Kategori" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error" placeholder="Kosongkan jika ingin membuat Kategori baru ....">
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">Nama Kategori</label>
								<input id="update_name" name="cc-nama" type="text" class="form-control cc-nama valid" data-val="true" data-val-required="Masukan Nama Barang" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
						</form>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button id="update" type="button" class="btn btn-primary">Update</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
			</div>
		</div>
	</div>
</div>