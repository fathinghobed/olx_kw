<div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				

				<div class="card">
					<div class="card-body">
						<div class="card-title">
							<h3 class="text-center title-2">Lihat User</h3>
						</div>
						<hr>
						<form action="" method="post" novalidate="novalidate">
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">ID</label>
								<input id="update_id" name="cc-nama" type="text" class="form-control cc-nama valid" data-val="true" data-val-required="Masukan Nama Barang" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error" readonly>
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">Nama</label>
								<input id="update_name" name="cc-nama" type="text" class="form-control cc-nama valid" data-val="true" data-val-required="Masukan Nama Barang" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">Username</label>
								<input id="update_username" name="cc-name" type="text" class="form-control cc-name valid" data-val="true" data-val-required="Masukan Kategori" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group">
								<label for="cc-number" class="control-label mb-1">Email</label>
								<input id="update_email" name="cc-number" type="text" class="form-control cc-number identified visa" value="" data-val="true" data-val-required="Masukan harga terlebih dahulu" autocomplete="cc-number">
								<span class="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group">
								<label for="cc-number" class="control-label mb-1">Bergabung Sejak</label>
								<input id="update_created_at" name="cc-number" type="text" class="form-control cc-number identified visa" value="" data-val="true" data-val-required="Please enter the card number" data-val-cc-number="Please enter a valid card number" autocomplete="cc-number">
								<span class="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true"></span>
							</div>
						</form>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button id="update" type="button" class="btn btn-primary">Tambah</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
			</div>
		</div>
	</div>
</div>