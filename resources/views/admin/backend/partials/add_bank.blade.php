<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				

				<div class="card">
					<div class="card-body">
						<div class="card-title">
							<h3 class="text-center title-2">Tambah Bank</h3>
						</div>
						<hr>
						<form id="tambahdata" method="POST" novalidate="novalidate" enctype="multipart/form-data">
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">Nama Bank</label>
								<input id="name" name="name" type="text" class="form-control cc-nama valid" data-val="true" data-val-required="Masukan Nama Barang" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">Code Bank</label>
								<input id ="code" name="code" type="text" class="form-control cc-nama valid" data-val="true" data-val-required="Masukan Nama Barang" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group has-success">
								<label for="cc-number" class="control-label mb-1">Gambar</label>
								<input type="file" id="image" name="image" class="form-control-file">
							</div>
							<div>
								<button id="tambahkan" type="submit" class="btn btn-primary btn-block">
									Tambah Data
								</button>
								<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
									Batalkan
								</button>

							</div>
						</form>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				

				<div class="card">
					<div class="card-body">
						<div class="card-title">
							<h3 class="text-center title-2">Update Bank</h3>
						</div>
						<hr>
						<form id="updatedata" method="POST" novalidate="novalidate" enctype="multipart/form-data">
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">ID Bank</label>
								<input id="update_id" name="update_id" type="text" class="form-control cc-nama valid" data-val="true" data-val-required="Masukan Nama Barang" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error" readonly>
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">Code Bank</label>
								<input id="update_code" name="update_code" type="text" class="form-control cc-nama valid" data-val="true" data-val-required="Masukan Nama Barang" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error" readonly>
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">Nama Bank</label>
								<input id="update_name" name="update_name" type="text" class="form-control cc-nama valid" data-val="true" data-val-required="Masukan Nama Barang" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">Gambar</label>
								<img id="img-data" src=""><br>
							</div>
							<div class="form-group has-success">
								<label for="cc-number" class="control-label mb-1">Upload</label>
								<input type="file" id="update_image" name="update_image" class="form-control-file">
							</div>
							<div>
								<button id="update" type="submit" class="btn btn-primary btn-block">
									Update Data
								</button>
								<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
									Batalkan
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>