<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				

				<div class="card">
					<div class="card-body">
						<div class="card-title">
							<h3 class="text-center title-2">Tambah Barang</h3>
						</div>
						<hr>
						<form id="tambahdata" method="POST" novalidate="novalidate" enctype="multipart/form-data">
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">Nama Barang</label>
								<input id="name" name="name" type="text" class="form-control cc-nama valid" data-val="true" data-val-required="Masukan Nama Barang" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">Kategori</label>
								<input id="category_id" name="category_id" type="text" class="form-control cc-name valid" data-val="true" data-val-required="Masukan Kategori" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group">
								<label for="cc-number" class="control-label mb-1">Harga</label>
								<input id="harga" name="harga" type="text" class="form-control cc-number identified visa" value="" data-val="true" data-val-required="Masukan harga terlebih dahulu" autocomplete="cc-number">
								<span class="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group">
								<label for="cc-number" class="control-label mb-1">Satuan</label>
								<input id="satuan" name="satuan" type="text" class="form-control cc-number identified visa" value="" data-val="true" data-val-required="Please enter the card number" data-val-cc-number="Please enter a valid card number" autocomplete="cc-number">
								<span class="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group">
								<label for="cc-number" class="control-label mb-1">Stok</label>
								<input id="stok" name="stok" type="text" class="form-control cc-number identified visa" value="" data-val="true" data-val-required="Please enter the card number" data-val-cc-number="Please enter a valid card number" autocomplete="cc-number">
								<span class="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group">
								<label for="cc-number" class="control-label mb-1">Deskripsi</label>
								<input id="deskripsi" name="deskripsi" type="text" class="form-control cc-number identified visa" value="" data-val="true" data-val-required="Please enter the card number" data-val-cc-number="Please enter a valid card number" autocomplete="cc-number">
								<span class="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group has-success">
								<label for="cc-number" class="control-label mb-1">Gambar</label>
								<input type="file" id="image" name="image" multiple="" class="form-control-file">
							</div>
							<div>
								<button id="tambahkan" type="submit" class="btn btn-primary btn-block">
									Tambah Data
								</button>
								<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
									Batalkan
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="card">
					<div class="card-body">
						<div class="card-title">
							<h3 class="text-center title-2">Update Barang</h3>
						</div>
						<hr>
						<form id="updatedata" method="POST" novalidate="novalidate" enctype="multipart/form-data">
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">ID</label>
								<input id="update_id" name="update_id" type="text" class="form-control cc-nama valid" data-val="true" data-val-required="Masukan Nama Barang" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error" readonly>
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">Nama Barang</label>
								<input id="update_name" name="update_name" type="text" class="form-control cc-nama valid" data-val="true" data-val-required="Masukan Nama Barang" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">Kategori</label>
								<input id="update_category_id" name="update_category_id" type="text" class="form-control cc-name valid" data-val="true" data-val-required="Masukan Kategori" autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
								<span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group">
								<label for="cc-number" class="control-label mb-1">Harga</label>
								<input id="update_harga" name="update_harga" type="text" class="form-control cc-number identified visa" value="" data-val="true" data-val-required="Masukan harga terlebih dahulu" autocomplete="cc-number">
								<span class="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group">
								<label for="cc-number" class="control-label mb-1">Satuan</label>
								<input id="update_satuan" name="update_satuan" type="text" class="form-control cc-number identified visa" value="" data-val="true" data-val-required="Please enter the card number" data-val-cc-number="Please enter a valid card number" autocomplete="cc-number">
								<span class="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group">
								<label for="cc-number" class="control-label mb-1">Stok</label>
								<input id="update_stok" name="update_stok" type="text" class="form-control cc-number identified visa" value="" data-val="true" data-val-required="Please enter the card number" data-val-cc-number="Please enter a valid card number" autocomplete="cc-number">
								<span class="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group">
								<label for="cc-number" class="control-label mb-1">Deskripsi</label>
								<input id="update_deskripsi" name="update_deskripsi" type="text" class="form-control cc-number identified visa" value="" data-val="true" data-val-required="Please enter the card number" data-val-cc-number="Please enter a valid card number" autocomplete="cc-number">
								<span class="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true"></span>
							</div>
							<div class="form-group has-success">
								<label for="cc-name" class="control-label mb-1">Gambar</label>
								<img id="img-data" src=""><br>
							</div>
							<div class="form-group has-success">
								<label for="cc-number" class="control-label mb-1">Upload</label>
								<input type="file" id="update_image" name="update_image" multiple="" class="form-control-file">
							</div>
							<div>
								<button id="update" type="submit" class="btn btn-primary btn-block">
									Update Data
								</button>
								<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
									Batalkan
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>