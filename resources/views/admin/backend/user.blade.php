@extends('admin.layout.app')

@section('content')
<div class="section__content section__content--p30">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="table-responsive m-b-40">
                    <table id="tablegua" class="table table-borderless table-data3">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Username</th>
                                <th>Tanggal</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="showbray">
                            
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

@include('admin.backend.partials.show_user')
@endsection

@section('script')
<script>
    $(document).ready(function(){

        tampil();

        $('#tablegua').dataTable();

        function tampil(){
            $.ajax({
                type  : 'GET',
                url   : "{{ url('/api/v1/user/all') }}",
                async : false,
                success: function(data){
                    var html = '';
                    $.each(data.data, function(i, obj){
                        html += '<tr>'; 
                        html += '<td>'+obj.id+'</td>';
                        html += '<td>'+obj.name+'</td>';
                        html += '<td>'+obj.email+'</td>';
                        html += '<td>'+obj.username+'</td>';
                        html += '<td>'+obj.created_at+'</td>'+
                        '<td><a href="javascript:;" class="btn btn-info btn-xs item_edit fa fa-eye" data="'+obj.id+'"></a>';
                    })
                    $('#showbray').append(html);
                }
            })
        }

        $(".item_edit").click(function(){
            $c = $(this).attr('data');
            $.ajax({
                type    : 'GET',
                url     : "{{ url('/api/v1/user/'.Auth::user()->id.'/show') }}/"+$c,
                async   : false,
                success : function(data){
                    $('#showModal').modal('show');
                    $.each(data.data, function(i, obj){
                        document.getElementById('update_id').value = obj.id;
                        document.getElementById('update_name').value = obj.name;
                        document.getElementById('update_username').value = obj.username;
                        document.getElementById('update_email').value = obj.email;
                        document.getElementById('update_created_at').value = obj.created_at;
                    });
                    
                    
                }
            })
        });
    })
</script>

@endsection
