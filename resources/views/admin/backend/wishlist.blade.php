@extends('admin.layout.app')

@section('content')
<div class="section__content section__content--p30">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="table-responsive m-b-40">
                    <table id="tablegua" class="table table-borderless table-data3">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>Nama Product</th>
                                <th>Tanggal</th>
                            </tr>
                        </thead>
                        <tbody id="showbray" ></tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function(){

        tampil();
        $('#tablegua').dataTable();
        function tampil(){
            $.ajax({
                type  : 'GET',
                url   : "{{ url('/api/v1/wishlist/all') }}",
                async : false,
                success: function(data){
                    var html = '';
                    $.each(data.data, function(i, obj){
                        html += '<tr>'; 
                        html += '<td>'+obj.id+'</td>';
                        html += '<td>'+obj.user_id+'</td>';
                        html += '<td>'+obj.product_id+'</td>';
                        html += '<td>'+obj.created_at+'</td>';
                    })
                    $('#showbray').append(html);
                }
            })
        } 
    })
</script>

@endsection
