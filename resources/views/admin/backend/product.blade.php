@extends('admin.layout.app')

@section('content')
<div class="section__content section__content--p30">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="table-data__tool">
					<div class="table-data__tool-right">
						<button class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#mediumModal">
							<i class="zmdi zmdi-plus"></i>Tambah Barang
						</button>
					</div>
				</div>
				<div class="table-responsive m-b-40">
					<table id="tablegua" class="table table-borderless table-data3">
						<thead>
							<tr>
								<th>ID</th>
								<th>Nama</th>
								<th>Kategori</th>
								<th>Harga</th>
								<th>Stok</th>
								<th>Satuan</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="showbray">
							
						</tbody>
					</table>
				</div>

			</div>
		</div>
	</div>
</div>

@include('admin.backend.partials.add_product')

@endsection

@section('script')
<script>
	$(document).ready(function(){
		tampil();
		$('#tablegua').dataTable();
		function tampil(){
			$.ajax({
				type  : 'GET',
				url   : "{{ url('/api/v1/product/all') }}",
				async : false,
				success: function(data){
					var html = '';
					$.each(data.data, function(i, obj){
						html += '<tr>'; 
						html += '<td>'+obj.id+'</td>';
						html += '<td>'+obj.name+'</td>';
						html += '<td>'+obj.category+'</td>';
						html += '<td>'+obj.harga+'</td>';
						html += '<td>'+obj.stok+'</td>';
						html += '<td>'+obj.satuan+'</td>'+
						'<td><a href="javascript:;" class="btn btn-info btn-xs item_edit fa fa-pencil" data="'+obj.id+'"></a><a href="javascript:;" class="btn btn-danger btn-xs item_hapus fa fa-trash" data="'+obj.id+'"></a></td>';
					})
					$('#showbray').append(html);
				}
			})
		}

		$("#tambahdata").submit(function(e){
			e.preventDefault();
			var data = new FormData(this);
			$.ajax({
				type 	: 'POST',
				url 	: "{{ url('/api/v1/product/'.Auth::user()->id.'/store') }}",
				data 	: data,
				processData: false,
				contentType: false,
				success : function(data){
					$('#mediumModal').modal('hide');
					document.getElementById('name').value= null;
					document.getElementById('category_id').value= null;
					document.getElementById('harga').value= null;
					document.getElementById('satuan').value= null;
					document.getElementById('stok').value= null;
					document.getElementById('deskripsi').value= null;
					document.getElementById('image').value= null;
					Swal.fire({
						icon: 'success',
						title: data.msg,
						showConfirmButton: false,
						timer: 1500
					});
					tampil();
				}
			})

		});

		$(".item_edit").click(function(){
			$c = $(this).attr('data');
			$.ajax({
				type 	: 'GET',
				url 	: "{{ url('/api/v1/product/'.Auth::user()->id.'/show') }}/"+$c,
				async	: false,
				success : function(data){
					$('#updateModal').modal('show');
					$.each(data.data, function(i, obj){
						document.getElementById('update_id').value = obj.id;
						document.getElementById('update_name').value = obj.name;
						document.getElementById('update_category_id').value = obj.category_id;
						document.getElementById('update_harga').value = obj.harga;
						document.getElementById('update_satuan').value = obj.satuan;
						document.getElementById('update_stok').value = obj.stok;
						document.getElementById('update_deskripsi').value = obj.deskripsi;
						$('#img-data').attr('src', "{{ asset('storage/UploadImages') }}/"+obj.image);
					});
				}
			})
		});

		$("#updatedata").submit(function(e){
			e.preventDefault();
			var data = new FormData(this);
			let id = $('#update_id').val();
			$.ajax({
				type 	: 'POST',
				url 	: "{{ url('/api/v1/product/'.Auth::user()->id.'/update') }}/"+id,
				data 	: data,
				processData: false,
				contentType: false,
				success : function(data){
					$('#updateModal').modal('hide');
					Swal.fire({
						icon: 'success',
						title: data.msg,
						showConfirmButton: false,
						timer: 1500
					});
					document.getElementById('update_image').value = null;
					window.location.reload();
				}
			})
		});

		$(".item_hapus").click(function(){
			$c = $(this).attr('data');
			Swal.fire({
				icon: 'warning',
				title: 'Anda yakin ?',
				text: 'data yang telah anda hapus tidak dapat dikembalikan',
				showCancelButton: true,
				confirmButtonText: 'Delete',
				cencelButtonText: 'Cencel',
			}).then((result) => {
				if (result.value) {
					$.ajax({
						type: 'POST',
						url: "{{ url('/api/v1/product/'.Auth::user()->id.'/destroy') }}/"+$c,
						error: function() {
							alert('Something is wrong');
						},
						success: function(data) {
							Swal.fire({
								icon: 'success',
								title: data.msg,
								showConfirmButton: false,
								timer: 1500
							});
							window.location.reload()
						}
					});
				}
			})
		});
	})
</script>
})
</script>
@endsection
