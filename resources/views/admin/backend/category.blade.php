@extends('admin.layout.app')

@section('content')
<div class="section__content section__content--p30">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="table-data__tool">
					<div class="table-data__tool-right">
						<button class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#mediumModal">
							<i class="zmdi zmdi-plus"></i>Tambah Kategori
						</button>
					</div>
				</div>

				<div class="table-responsive m-b-40">
					<table id="tablegua" class="table table-borderless table-data3">
						<thead>
							<tr>
								<th>ID</th>
								<th>Parent ID</th>
								<th>Nama Kategori</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="showbray">
							
						</tbody>
					</table>
				</div>

			</div>
		</div>
	</div>
</div>
@endsection

@include('admin.backend.partials.add_category')

@section('script')
<script>
	$(document).ready(function(){
		
		tampil();
		$('#tablegua').dataTable();
		function tampil(){
			$.ajax({
				type  : 'GET',
				url   : "{{ url('/api/v1/category/all') }}",
				async : false,
				success: function(data){
					var html = '';
					$.each(data.data, function(i, obj){
						html += '<tr>'; 
						html += '<td>'+obj.id+'</td>';
						html += '<td>'+obj.parent_id+'</td>';
						html += '<td>'+obj.name+'</td>'+
						'<td><a href="javascript:;" class="btn btn-info btn-xs item_edit fa fa-pencil" data="'+obj.id+'"></a><a href="javascript:;" class="btn btn-danger btn-xs item_hapus fa fa-trash" data="'+obj.id+'"></a></td>';
					})
					$('#showbray').append(html);
				}
			})
		}

		$("#tambahkan").click(function(){
			$a = $('#name').val();
			$b = $('#parent_id').val();
			$.ajax({
				type 	: 'POST',
				url 	: "{{ url('/api/v1/category/'.Auth::user()->id.'/store') }}",
				data 	: {
					'name' 			: $a,
					'parent_id' 	: $b,
				},
				success : function(data){
					$('#mediumModal').modal('hide');
					document.getElementById('name').value= null;
					document.getElementById('parent_id').value= null;
					Swal.fire({
						icon: 'success',
						title: data.msg,
						showConfirmButton: false,
						timer: 1500
					});

					window.location.reload()
				}
			})
		});

		$(".item_edit").click(function(){
			$c = $(this).attr('data');
			console.log($c);
			$.ajax({
				type 	: 'GET',
				url 	: "{{ url('/api/v1/category/'.Auth::user()->id.'/show') }}/"+$c,
				async	: false,
				success : function(data){
					$('#updateModal').modal('show');
					$.each(data.data, function(i, obj){
						document.getElementById('update_name').value = obj.name;
						document.getElementById('update_id').value = obj.id;
						document.getElementById('update_parent_id').value = obj.parent_id;
					});
					
					
				}
			})
		});

		$("#update").click(function(){
			$a = $('#update_name').val();
			$b = $('#update_parent_id').val();
			$c = $('#update_id').val();
			$.ajax({
				type 	: 'POST',
				url 	: "{{ url('/api/v1/category/'.Auth::user()->id.'/update') }}/"+$c,
				data 	:{
					'name' : $a,
					'parent_id' : $b,
					'id' : $c,
				},
				success : function(data){
					$('#updateModal').modal('hide');

					Swal.fire({
						icon: 'success',
						title: data.msg,
						showConfirmButton: false,
						timer: 1500
					});

				}
			})
		});

		$(".item_hapus").click(function(){
			$c = $(this).attr('data');
			Swal.fire({
				icon: 'warning',
				title: 'Anda yakin ?',
				text: 'data yang telah anda hapus tidak dapat dikembalikan',
				showCancelButton: true,
				confirmButtonText: 'Delete',
				cencelButtonText: 'Cencel',
			}).then((result) => {
				if (result.value) {
					$.ajax({
						type: 'POST',
						url: "{{ url('/api/v1/category/'.Auth::user()->id.'/destroy') }}/"+$c,
						error: function() {
							alert('Something is wrong');
						},
						success: function(data) {
							Swal.fire({
								icon: 'success',
								title: data.msg,
								showConfirmButton: false,
								timer: 1500
							});
							window.location.reload()
							
						}
					});
				}
			})
		});
	})
</script>

@endsection
