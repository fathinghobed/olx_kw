<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Required meta tags-->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Dashboard</title>
	<link rel="stylesheet" href="{{ asset('css/font-face.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/font-awesome-4.7/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/font-awesome-5/css/fontawesome-all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/mdi-font/css/material-design-iconic-font.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/bootstrap-4.1/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/animsition/animsition.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/wow/animate.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/css-hamburgers/hamburgers.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/slick/slick.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/perfect-scrollbar/perfect-scrollbar.css') }}">
	<link rel="stylesheet" href="{{ asset('css/theme.css') }}">
	<link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">


	@yield('style')

</head>
<body class="">

	<div class="page-wrapper">
		@include('admin.layout.header')
		@include('admin.layout.sidebar')
		<div class="page-container">
			@include('admin.layout.navbar')
			<div class="main-content">
				@yield('content')
			</div>
		</div>
	</div>

	<script src="js/main.js"></script>

	<script src="{{ asset('vendor/jquery-3.2.1.min.js') }}"></script>
	<script src="{{ asset('vendor/bootstrap-4.1/popper.min.js') }}"></script>
	<script src="{{ asset('vendor/bootstrap-4.1/bootstrap.min.js') }}"></script>
	<script src="{{ asset('vendor/slick/slick.min.js') }}"></script>
	<script src="{{ asset('vendor/wow/wow.min.js') }}"></script>
	<script src="{{ asset('vendor/animsition/animsition.min.js') }}"></script>
	<script src="{{ asset('vendor/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
	<script src="{{ asset('vendor/counter-up/jquery.waypoints.min.js') }}"></script>
	<script src="{{ asset('vendor/counter-up/jquery.counterup.min.js') }}"></script>
	<script src="{{ asset('vendor/circle-progress/circle-progress.min.js') }}"></script>
	<script src="{{ asset('vendor/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
	<script src="{{ asset('vendor/chartjs/Chart.bundle.min.js') }}"></script>
	<script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
	<script src="{{ asset('js/main.js')}}"></script>
	<script src="{{ asset('js/datatables.min.js')}}"></script>
	<script src="{{ asset('js/sweetalert2.min.js')}}"></script>

	@yield('script')

</body>

</html>
<!-- end document-->