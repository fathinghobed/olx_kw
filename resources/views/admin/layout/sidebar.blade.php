<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <a href="#">
            <img src="{{ asset('images/icon/logo.png') }}" alt="Cool Admin" />
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">
                <li class="has-sub">
                    <a href="{{ route('admin') }}">
                        <i class="fas fa-tachometer-alt"></i>Dashboard
                    </a>
                </li>
                <li class="has-sub">
                    <a href="{{ route('admin.user') }}">
                        <i class="fas fa-chart-bar"></i>User
                    </a>
                </li>
                <li class="has-sub">
                    <a href="{{ route('admin.category') }}">
                        <i class="fas fa-table"></i>Category
                    </a>
                </li>
                <li class="has-sub">
                    <a href="{{ route('admin.product') }}">
                        <i class="far fa-check-square"></i>Product
                    </a>
                </li>
                <li class="has-sub">
                    <a href="{{ route('admin.bank') }}">
                        <i class="fas fa-calendar-alt"></i>Bank
                    </a>
                </li>
                <li class="has-sub">
                    <a href="{{ route('admin.wishlist') }}">
                        <i class="fas fa-map-marker-alt"></i>Wishlist
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>