  <header class="header-mobile d-block d-lg-none">
    <div class="header-mobile__bar">
        <div class="container-fluid">
            <div class="header-mobile-inner">
                <a class="logo" href="{{route('admin')}}">
                    <img src="{{ asset('images/icon/logo.png') }}" alt="OLEX" />
                </a>
                <button class="hamburger hamburger--slider" type="button">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <nav class="navbar-mobile">
        <div class="container-fluid">
            <ul class="navbar-mobile__list list-unstyled">
                <li class="has-sub">
                    <a href="{{ route('admin') }}">
                        <i class="fas fa-tachometer-alt"></i>Dashboard
                    </a>
                </li>
                <li class="has-sub">
                    <a href="{{ route('admin.user') }}">
                        <i class="fas fa-chart-bar"></i>User
                    </a>
                </li>
                <li class="has-sub">
                    <a href="{{ route('admin.category') }}">
                        <i class="fas fa-table"></i>Category
                    </a>
                </li>
                <li class="has-sub">
                    <a href="{{ route('admin.product') }}">
                        <i class="far fa-check-square"></i>Product
                    </a>
                </li>
                <li class="has-sub">
                    <a href="{{ route('admin.bank') }}">
                        <i class="fas fa-calendar-alt"></i>Bank
                    </a>
                </li>
                <li class="has-sub">
                    <a href="{{ route('admin.wishlist') }}">
                        <i class="fas fa-map-marker-alt"></i>Wishlist
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>