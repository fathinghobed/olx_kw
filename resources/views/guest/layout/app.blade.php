<!DOCTYPE html>
<html lang="zxx">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>OLX KW SUPER</title>

	<link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="{{ asset('user/css/bootstrap.min.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ asset('user/css/font-awesome.min.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ asset('user/css/elegant-icons.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ asset('user/css/nice-select.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ asset('user/css/jquery-ui.min.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ asset('user/css/owl.carousel.min.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ asset('user/css/slicknav.min.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ asset('user/css/style.css') }}" type="text/css">

</head>

<body>

	@include('guest.layout.preloader')
	@include('guest.layout.header')
	@include('guest.layout.navbar')
	@yield('content')
	@include('guest.layout.footer') 

	<script src="{{ asset('user/js/jquery-3.3.1.min.js') }}"></script>
	<script src="{{ asset('user/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('user/js/jquery.nice-select.min.js') }}"></script>
	<script src="{{ asset('user/js/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('user/js/jquery.slicknav.js') }}"></script>
	<script src="{{ asset('user/js/mixitup.min.js') }}"></script>
	<script src="{{ asset('user/js/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('user/js/main.js') }}"></script>
	
	@yield('script')

</body>
</html>