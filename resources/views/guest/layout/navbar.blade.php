 <header class="header" style="margin-bottom: 10px; margin-top: 10px">
    <div class="header__top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__left">
                        <ul>
                            <li><i></i>OLX KW SUPER TITIK KOM</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__right">
                        <div class="header__top__right__social">
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-shopping-bag"></i></a>
                            <a href="#"><i class="fa fa-envelope"></i></a>
                        </div>
                        <div class="header__top__right__auth">
                            <a href="{{route('login')}}"><i class="fa fa-user"></i> Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>