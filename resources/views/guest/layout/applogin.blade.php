<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
	<!-- <link rel="icon" type="image/png" href="../assets/img/favicon.png"> -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>
		OLX KW
	</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/font-face.css') }}">
	<link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/mdi-font/css/material-design-iconic-font.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/animsition/animsition.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/wow/animate.css') }}" rel="stylesheet" media="all">
	<link rel="stylesheet" href="{{ asset('vendor/css-hamburgers/hamburgers.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/slick/slick.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/perfect-scrollbar/perfect-scrollbar.css') }}">
	<link rel="stylesheet" href="{{ asset('css/theme.css') }}">


</head>
<body>

	<div class="row">

	</div>
	<div class="row">
		
	</div>
	@yield('content')

	<!-- Jquery JS-->
	<!-- Bootstrap JS-->
	<!-- Vendor JS       -->
	<script src="{{ asset('js/jquery.min.js') }}"></script>
	<script src="{{ asset('vendor/slick/slick.min.js') }}"></script>
	<script src="{{ asset('vendor/wow/wow.min.js') }}"></script>
	<script src="{{ asset('vendor/animsition/animsition.min.js') }}"></script>
	<script src="{{ asset('vendor/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
	<script src="{{ asset('vendor/counter-up/jquery.waypoints.min.js') }}"></script>
	<script src="{{ asset('vendor/counter-up/jquery.counterup.min.js') }}"></script>
	<script src="{{ asset('vendor/circle-progress/circle-progress.min.js') }}"></script>
	<script src="{{ asset('vendor/chartjs/Chart.bundle.min.js') }}"></script>
	<script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
	<script src="{{ asset('js/popper.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/perfect-scrollbar.jquery.min.js') }}"></script>
	<script src="{{ asset('js/chartjs.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap-notify.js') }}"></script>
	<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
	<script src="{{ asset('js/datatables.min.js') }}"></script>
	<script src="{{ asset('js/main.js') }}"></script>

	@yield('script')
</body>
</html>