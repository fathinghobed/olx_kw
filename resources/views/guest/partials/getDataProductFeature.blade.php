<section class="featured spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2>Featured Product</h2>
                </div>
                <div class="featured__controls">
                    <ul>
                        <li class="active" data-filter="*">All</li>
                        @foreach($data as $a)
                        <li data-filter=".{{ $a['category'] }}">{{ $a['category'] }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="row featured__filter">
            @foreach($data as $a)
            <div class="col-lg-3 col-md-4 col-sm-6 mix {{$a['category']}} ">
                <div class="featured__item">
                    <div class="featured__item__pic set-bg">
                        <img src="{{ asset('storage/UploadImages/'.$a['image']) }}" alt="foto" width="200" height="200">
                        <ul class="featured__item__pic__hover">
                            <li><a href="#"><i class="fa fa-heart"></i></a></li>
                            <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                            <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                        </ul>
                    </div>
                    <div class="featured__item__text">
                        <h6><a href="#">{{ $a['name'] }}</a></h6>
                        <h5>{{ $a['harga'] }}</h5>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>