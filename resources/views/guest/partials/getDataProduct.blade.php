@foreach($data as $a)
<div class="col-lg-3 col-md-4 col-sm-6 mix oranges fresh-meat">
    <div class="featured__item">
        <div class="featured__item__pic set-bg">
            <img src="{{ asset('storage/UploadImages/'.$a['image']) }}" alt="foto" width="300" height="200">
            <ul class="featured__item__pic__hover">
                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
            </ul>
        </div>
        <div class="featured__item__text">
            <h6><a href="#">{{ $a['name'] }}</a></h6>
            <h5>{{ $a['harga'] }}</h5>
        </div>
    </div>
</div>
@endforeach