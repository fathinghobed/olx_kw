@extends('guest.layout.applogin')

@section('content')
<div class="page-wrapper">
    <div class="page-content--bge5">
        <div class="container">
            <div class="login-wrap">
                <div class="login-content">
                    <div class="login-logo">
                        <a href="#">
                            <h4>OLX KW SUPER</h4>
                        </a>
                    </div>
                    <div class="login-form">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="form-group">
                                <label>Nama</label>
                                <input class="au-input au-input--full" type="text" name="name" id="name" placeholder="Nama">
                            </div>
                            <div class="form-group">
                                <label>Username</label>
                                <input class="au-input au-input--full" type="text" name="username" id="username" placeholder="Username">
                            </div>
                            <div class="form-group">
                                <label>Email Address</label>
                                <input class="au-input au-input--full" type="email" name="email" id="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input class="au-input au-input--full" type="password" name="password" id="password" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input id="password-confirm" type="password" class="input100 @error('password') is-invalid @enderror au-input au-input--full" name="password_confirmation" placeholder="Confirm Password">
                            </div>
                            <div class="login-checkbox">
                                <label>
                                    <input type="checkbox" name="aggree">Agree the terms and policy
                                </label>
                            </div>
                            <div class="form-group">
                               <button type="submit" class="au-btn au-btn--block au-btn--green m-b-20">register</button>
                            </div>
                        </form>
                        <div class="register-link">
                            <p>
                                Sudah punya akun ?
                                <a href="{{ route('login') }}">Masuk disini</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection