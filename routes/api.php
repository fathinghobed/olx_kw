<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'v1'], function(){



	Route::get('/user/all', ['as' => 'user.all', 'uses' => 'API\UserController@index']);
	Route::get('/user/{user_id}/show', ['as' => 'user.show', 'uses' => 'API\UserController@show']);
	Route::get('/user/{user_id}/show/{id}', ['as' => 'user.admin.show', 'uses' => 'API\UserController@admin']);
	Route::post('/user/{user_id}/store', ['as' => 'user.store', 'uses' => 'API\UserController@store']);
	Route::post('/user/{user_id}/update', ['as' => 'user.update', 'uses' => 'API\UserController@update']);

// product
	Route::get('/product/all', ['as' => 'product.index', 'uses' => 'API\ProductController@index']);
	Route::get('/product/searchByAll', ['as' => 'product.searchByAll', 'uses' => 'API\ProductController@searchByAll']);
	Route::get('/product/{user_id}/show/{product_id}', ['as' => 'product.show', 'uses' => 'API\ProductController@show']);
	Route::post('/product/{user_id}/store', ['as' => 'product.store', 'uses' => 'API\ProductController@store']);
	Route::post('/product/{user_id}/update/{product_id}', ['as' => 'product.update', 'uses' => 'API\ProductController@update']);
	Route::post('/product/{user_id}/destroy/{product_id}', ['as' => 'product.destroy', 'uses' => 'API\ProductController@destroy']);


// bank
	Route::get('/bank/all', ['as' => 'bank.index', 'uses' => 'API\BankController@index']);
	Route::get('/bank/{user_id}/show/{bank_id}', ['as' => 'bank.show', 'uses' => 'API\BankController@show']);
	Route::post('/bank/{user_id}/store', ['as' => 'bank.store', 'uses' => 'API\BankController@store']);
	Route::post('/bank/{user_id}/update/{bank_id}', ['as' => 'bank.update', 'uses' => 'API\BankController@update']);
	Route::post('/bank/{user_id}/destroy/{bank_id}', ['as' => 'bank.destroy', 'uses' => 'API\BankController@destroy']);


// category
	Route::get('/category/all', ['as' => 'category.index', 'uses' => 'API\CategoryController@index']);
	Route::get('/category/onlyasc', ['as' => 'category.only.asc', 'uses' => 'API\CategoryController@onlycategoryasc']);
	Route::get('/category/onlydesc', ['as' => 'category.only.desc', 'uses' => 'API\CategoryController@onlycategorydesc']);
	Route::get('/category/forhome', ['as' => 'category.forhome', 'uses' => 'API\CategoryController@forhome']);
	Route::get('/category/get/{category_id}', ['as' => 'category.get', 'uses' => 'API\CategoryController@get']);
	Route::get('/category/searchByCategory/{category_id}', ['as' => 'category.searchByCategory', 'uses' => 'API\CategoryController@searchByCategory']);
	Route::get('/category/{user_id}/show/{category_id}', ['as' => 'category.show', 'uses' => 'API\CategoryController@show']);
	Route::post('/category/{user_id}/store', ['as' => 'category.store', 'uses' => 'API\CategoryController@store']);
	Route::post('/category/{user_id}/update/{category_id}', ['as' => 'category.update', 'uses' => 'API\CategoryController@update']);
	Route::post('/category/{user_id}/destroy/{category_id}', ['as' => 'category.destroy', 'uses' => 'API\CategoryController@destroy']);

//conversation 
	Route::get('/conversation/all', ['as' => 'conversation.index', 'uses' => 'API\ConversationController@index']);
	Route::get('/conversation/{	user_id}/show/{conversation_id}', ['as' => 'conversation.show', 'uses' => 'API\ConversationController@show']);
	Route::post('/conversation/{user_id}/store', ['as' => 'conversation.store', 'uses' => 'API\ConversationController@store']);
	Route::post('/conversation/{user_id}/update/{conversation_id}', ['as' => 'conversation.update', 'uses' => 'API\ConversationController@update']);
	Route::post('/conversation/{user_id}/destroy/{conversation_id}', ['as' => 'conversation.destroy', 'uses' => 'API\ConversationController@destroy']);

//wishlist
	Route::get('/wishlist/all', ['as' => 'wishlist.all', 'uses' => 'API\WishlistController@all']);
	Route::get('/wishlist/{user_id}/index', ['as' => 'wishlist.index', 'uses' => 'API\WishlistController@index']);
	Route::get('/wishlist/{user_id}/show/{wishlist_id}', ['as' => 'wishlist.show', 'uses' => 'API\WishlistController@show']);
	Route::post('/wishlist/{user_id}/store', ['as' => 'wishlist.store', 'uses' => 'API\WishlistController@store']);
	Route::post('/wishlist/{user_id}/destroy/{wishlist_id}', ['as' => 'wishlist.destroy', 'uses' => 'API\WishlistController@destroy']);

//inquiry
	Route::get('/inquiry/all', ['as' => 'inquiry.all', 'uses' => 'API\InquiryController@all']);
	Route::get('/inquiry/{user_id}/index', ['as' => 'inquiry.index', 'uses' => 'API\InquiryController@index']);
	Route::get('/inquiry/{user_id}/show/{inquiry_id}', ['as' => 'inquiry.show', 'uses' => 'API\InquiryController@show']);
	Route::post('/inquiry/{user_id}/store', ['as' => 'inquiry.store', 'uses' => 'API\InquiryController@store']);
	Route::post('/inquiry/{user_id}/update/{inquiry_id}', ['as' => 'inquiry.update', 'uses' => 'API\InquiryController@update']);
	Route::post('/inquiry/{user_id}/destroy/{inquiry_id}', ['as' => 'inquiry.destroy', 'uses' => 'API\InquiryController@destroy']);

});

