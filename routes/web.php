<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/logout', 'HomeController@logout')->name('lologout');


Route::get('/', function(){
	return view('guest.index');
})->name('guest.index');

Route::get('/login', function(){
	return view('auth.login');
})->name('guest.login');

Route::get('/detail', function(){
	return view('guest.detail');
})->name('guest.detail');

Auth::routes();

Route::get('/admin', function(){
	return view('admin.backend.index');
})->name('admin');
Route::get('/admin/user', function(){
	return view('admin.backend.user');
})->name('admin.user');
Route::get('/admin/category', function(){
	return view('admin.backend.category');
})->name('admin.category');
Route::get('/admin/product', function(){
	return view('admin.backend.product');
})->name('admin.product');
Route::get('/admin/bank', function(){
	return view('admin.backend.bank');
})->name('admin.bank');
Route::get('/admin/wishlist', function(){
	return view('admin.backend.wishlist');
})->name('admin.wishlist');