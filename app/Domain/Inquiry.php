<?php

namespace App\Domain;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Domain\Product;
use App\User;
use App\User_address;

class Inquiry extends Model
{

	use SoftDeletes;

	protected $table = 'inquiries';

	protected $fillable = [
		'user_id', 'alamat', 'alamat_id', 'quantity', 'product_price', 'total_fee', 'status'
	];

	public function inquiryuser()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	public function inquiryproduct()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	public function inquiryaddress()
	{
		return $this->belongsTo(User::class, 'user_id');
	}
}
