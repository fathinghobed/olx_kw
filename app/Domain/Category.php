<?php

namespace App\Domain;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Domain\Category\Entities\Category as SubCategory;
use App\Domain\Category\Entities\Category as ParentCategory;

use App\Domain\Product;

class Category extends Model
{

	use SoftDeletes;

	protected $table = 'categories';

	protected $fillable = [
		'parent_id', 'name', 
	];

	public function product(){
		return $this->hasMany(Product::class, 'category_id', 'id');
	}

	public function parentProducts()
	{
		return $this->belongsToMany(Product::class, 'categories', 'parent_id', 'id');
	}

	public function parent()
	{
		return $this->belongsTo(ParentCategory::class, 'parent_id', 'id');
	}

	public function subcategory()
	{
		return $this->hasMany(SubCategory::class, 'parent_id', 'id');
	}
}
