<?php

namespace App\Domain;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Domain\Bank;
use App\User;

class Bank_account extends Model
{

    use SoftDeletes;

    protected $table = 'bank_accounts';

    protected $fillable = [
    	'user_id', 'bank_id', 'nomor_bank', 'atasnama',
    ];

    public function bank()
    {
    	return $this->belongsTo(Bank::class, 'bank_id');
    }

    public function userbank()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }
}
