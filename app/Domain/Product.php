<?php

namespace App\Domain;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Domain\Product_gallery;
use App\Domain\Category;

class Product extends Model
{

	use SoftDeletes;

	protected $table = 'products';

	protected $fillable = [
		'user_id', 'category_id', 'code', 'name', 'harga', 'satuan', 'stok', 'deskripsi', 'created_at',
	];

	public function productuser(){
		return $this->belongsTo(User::class, 'user_id');
	}

	public function productgallery(){
		return $this->hasMany(Product_gallery::class, 'product_id');
	}

	public function productcategory(){
		return $this->belongsTo(Category::class, 'category_id');
	}
}
