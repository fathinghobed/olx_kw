<?php

namespace App\Domain;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Domain\Product;

class Product_gallery extends Model
{

	use SoftDeletes;

	protected $table = 'product_galleries';

	protected $fillable = [
		'product_id', 'name',
	];

	public function product(){
		return $this->belongsTo(Product::class, 'product_id');
	}
}
