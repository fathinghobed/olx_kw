<?php

namespace App\Domain;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Conversation extends Model
{

	use SoftDeletes;

	protected $table = 'conversations';

	protected $fillable = [
		'inquiry_id', 'product_id', 'from_user_id', 'to_user_id', 'message',
	];

	public function userchat(){
		return $this->belongsTo(User::class, 'user_id');
	}
}
