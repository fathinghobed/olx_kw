<?php

namespace App\Domain;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Domain\Product;

class Wishlist extends Model
{

	use SoftDeletes;

	protected $table = 'wishlists';

	protected $fillable = [
		'user_id', 'product_id',
	];

	public function wishlistuser(){
		return $this->belongsTo(User::class, 'user_id');
	}

	public function wishlistproduct(){
		return $this->belongsTo(Product::class, 'product_id');
	}
}
