<?php

namespace App\Domain;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Domain\Bank_account;

class Bank extends Model
{
	use SoftDeletes;
	
    protected $table = 'banks';

    protected $fillable = [
    	'code', 'name', 'image',
    ];

    public function bank_account()
    {
    	return $this->hasMany(Bank_account::class, 'bank_id');
    }
}
