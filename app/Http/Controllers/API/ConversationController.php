<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Conversation;

class ConversationController extends Controller
{
    protected $model;

    public function __construct(Conversation $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->model->all();

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Berhasil...!',
            'data'          => $data,
        ], 200); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $product_id = NULL, $inquiry_id, $user_id, $to_user_id)
    {
        $data = $this->model->create([
            'product_id'    => $product_id,
            'inquiry_id'    => $inquiry_id,
            'from_user_id'  => $user_id,
            'to_user_id'    => $to_user_id,
            'message'       => $request['message,'],
        ]);

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Berhasil...!',
            'data'          => $data,
        ], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $to_user_id)
    {
        $data = $this->model->where([
            ['from_user_id', '=', $user_id],
            ['to_user_id', '=', $to_user_id]
        ])->get();

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Berhasil...!',
            'data'          => $data,
        ], 200); 
    }
}
