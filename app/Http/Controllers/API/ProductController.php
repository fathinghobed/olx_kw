<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\CategoryController;
use App\Domain\Product;
use App\Http\Controllers\API\UploadkuHandler;


class ProductController extends Controller
{
    protected $upload;
    protected $model;

    public function __construct(Product $model, UploadkuHandler $upload)
    {
        $this->model = $model;
        $this->upload = $upload;
    }

    public function index()
    {
        $data = $this->model->with('productgallery', 'productcategory')->get();

        $dataku = self::productfactories($data);

        return response()->json([
            'status_code'   => 200,
            'msg'           => 'Data berhasil didapatkan...!',
            'data'          => $dataku,
        ], 200);
    }

    public function searchByall(){
        $data = $this->model->with('productgallery', 'productcategory')->get();
        $dataku = self::productfactories($data);
        return view('guest.partials.getDataProductFeature', ['data' => $dataku]);
    }

    public function findByCategory($category_id)
    {
        $data = $this->model->where('category_id', $category_id)->with('productgallery', 'productcategory')->get();

        $dataku = self::productfactories($data);

        return $dataku;
    }

    public function searchByKeyword(Request $request)
    {
        $data = $this->model->where('name', 'like', '%'.$request['name'].'%')->get();
        $data->paginate(10);

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Detail data',
            'data'          => $data,
        ], 200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store($user_id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'satuan' => 'required',
            'deskripsi' => 'required',
            'category_id' => 'required',
            'deskripsi' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg|max:1000'
        ]);
        $image = $this->upload->upload($request['image']);

        $data = $this->model->create([
            'user_id'       => $user_id,
            'category_id'   => $request['category_id'],
            'name'          => $request['name'],
            'harga'         => $request['harga'],
            'satuan'        => $request['satuan'],
            'stok'          => $request['stok'],
            'deskripsi'     => $request['deskripsi']
        ]);

        $data->productgallery()->create([
            'name'          => $image,
        ]);

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Data berhasil dibuat...!',
            'data'          => $data,
        ], 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($user_id, $product_id)
    {
        $data = $this->model->where('id', $product_id)->with('productgallery', 'productcategory')->get();

        $dataku = self::productfactories($data);

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Detail data',
            'data'          => $dataku,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update($user_id, Request $request, $product_id)
    {
        $this->validate($request, [
            'update_image' => 'image|mimes:jpg,png,jpeg|max:1000'
        ]);

        if($request['update_image'] != NULL){
            $data = $this->model->find($product_id);
            $data->productgallery()->where('product_id', $product_id)->delete();

            $image = $this->upload->upload($request['update_image']);
            $data->productgallery()->create([
                'name'          => $image,
            ]); 
            $data = $this->model->find($product_id);
            $data->update([
                'category_id'   => $request['update_category_id'],
                'name'          => $request['update_name'],
                'harga'         => $request['update_harga'],
                'satuan'        => $request['update_satuan'],
                'stok'          => $request['update_stok'],
                'deskripsi'     => $request['update_deskripsi']
            ]);
        }else{
            $data = $this->model->find($product_id);
            $data->update([
                'category_id'   => $request['update_category_id'],
                'name'          => $request['update_name'],
                'harga'         => $request['update_harga'],
                'satuan'        => $request['update_satuan'],
                'stok'          => $request['update_stok'],
                'deskripsi'     => $request['update_deskripsi']
            ]);
        }

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Detail berhasil diupdate...!',
            'data'          => $data,
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id, $product_id)
    {
        $data = $this->model->find($product_id);
        $data->productgallery()->where('product_id', $product_id)->delete();

        $data->delete();

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Detail berhasil dihapus...!',
            'data'          => $data,
        ], 200);
    }

    public function productfactories($data)
    {
        $dataku = [];
        foreach ($data as $a) {
            array_push($dataku,[
                'id'            => $a->id,
                'name'          => $a->name,
                'satuan'        => $a->satuan,
                'stok'          => $a->stok,
                'harga'         => $a->harga,
                'category_id'   => $a->category_id,
                'category'      => $a->productcategory->name,
                'image'         => $a->productgallery()->first()->name,
                'deskripsi'     => $a->deskripsi,
                'tanggal'       => $a->created_at,
            ]);
        };

        return $dataku;
    }

}
