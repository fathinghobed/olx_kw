<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Wishlist;

class WishlistController extends Controller
{

    protected $model;

    public function __construct(Wishlist $model)
    {
        $this->model = $model;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $data = $this->model->all();

        return response()->json([
            'status_code'   => 200,
            'msg'           => 'data berhasil diambil...!',
            'data'          => $data,
        ], 200);
    }

    public function index($user_id)
    {
        $data = $this->model->where('user_id', $user_id)->get();

        return response()->json([
            'status_code'   => 200,
            'msg'           => 'data berhasil diambil...!',
            'data'          => $data,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($user_id, Request $request)
    {
        $data = $this->model->create([
            'user_id'       => $user_id,
            'product_id'    => $request['product_id'],
        ]);

        return response()->json([
            'status_code'   => 200,
            'msg'           => 'data berhasil ditambahkan...!',
            'data'          => $data,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id, $wishlist_id)
    {
        $data = $this->model->where([
            // ['user_id', '=', $user_id],
            ['id', '=', $wishlist_id]
        ])->get();

        return response()->json([
            'status_code'   => 200,
            'msg'           => 'Detail data',
            'data'          => $data,
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id, $wishlist_id)
    {
        $data = $this->model->where([
            ['user_id', '=', $user_id],
            ['id', '=', $wishlist_id]
        ])->delete();

        return response()->json([
            'status_code'   => 200,
            'msg'           => 'data berhasil dihapus...!',
        ], 200);
    }
}
