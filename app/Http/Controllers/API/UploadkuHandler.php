<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Image;
use Illuminate\Support\Facades\Storage;

class UploadkuHandler
{

	protected $path = 'public/UploadImages';

	public function upload($data)
	{
		$imageName = NULL;
		$extension = $data->getClientOriginalExtension();
		$imageName = strtotime(date('Y-m-d H:i:s')).'_'.uniqid().'.'.$extension;
		$masuk = Storage::putFileAs($this->path, $data, $imageName);

		return $imageName;
	}

	public function deletegambar($data)
	{
		$hapus = Storage::delete($this->path.'/'.$data);

		if ($hapus) {
			return true;
		}else{
			return false;
		}
	}
}
