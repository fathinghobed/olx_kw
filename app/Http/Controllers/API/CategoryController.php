<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Category;
use App\Domain\Product;
use App\Http\Controllers\API\ProductController;

class CategoryController extends Controller
{

    protected $model;
    protected $model_product;

    public function __construct(Category $model, ProductController $model_product)
    {
        $this->model = $model;
        $this->model_product = $model_product;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function get($category_id){
        $data = $this->model_product->findByCategory($category_id);
        return view('guest.partials.getDataProduct', ['data' => $data]);
    }

    public function index()
    {
        $data = $this->model->all();

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Berhasil...!',
            'data'          => $data,
        ], 200);
    }

    public function onlycategoryasc()
    {
        $data = $this->model->where('parent_id', '=', NULL)->orderBy('created_at', 'asc')->get();

        return response()->json([   
            'status_code'   => '200',
            'msg'           => 'Berhasil...!',
            'data'          => $data,
        ], 200);
    }


    public function onlycategorydesc()
    {
        $data = $this->model->where('parent_id', '=', NULL)->orderBy('created_at', 'desc')->limit(4)->get();

        return response()->json([   
            'status_code'   => '200',
            'msg'           => 'Berhasil...!',
            'data'          => $data,
        ], 200);
    }

    public function forhome()
    {
        $data = $this->model->where('parent_id', '=', NULL)->orderBy('created_at', 'asc')->with('product')->limit(2)->get();

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Berhasil...!',
            'data'          => self::homeFactory($data)
        ]);

    }

    public function homeFactory($data)
    {
        $dataku = [];
        foreach ($data as $a) {
            array_push($dataku, [
                'id'        => $a->id,
                'name'      => $a->name,
                'product'   => self::homeProductOne($a->id)
            ]);
        }
        return $dataku;
    }

    public function homeProduct($data)
    {
        $dataku = [];
        foreach ($data as $a) {
            array_push($dataku, [
                'id'        => $a->id,
                'user_id'   => $a->user_id,
                'name'      => $a->name,
                'harga'     => $a->harga,
                'deskripsi' => $a->deskripsi,
            ]);
        }

        return $dataku;
    }

    public function homeProductOne($category_id){

        $data = Product::where('category_id', $category_id)->orderBy('created_at', 'desc')->with('productgallery', 'productcategory')->limit(2)->get();
        $dataku = [];
        foreach ($data as $a) {
            array_push($dataku, [
                'id'            => $a->id,
                'name'          => $a->name,
                'harga'         => $a->harga,
                'category'      => $a->productcategory->name,
                'image'         => $a->productgallery()->first()->name,
                'deskripsi'     => $a->deskripsi,
            ]);
        }

        return $dataku;
    }

    public function searchByCategory($category_id){
        $data = $this->model_product->findByCategory($category_id);
        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Berhasil...!',
            'data'          => $data,
        ], 200);
    }

    public function byCategory($data){
        $dataku = [];
        foreach ($data as $a) {
            array_push($dataku,[
                'id'            => $a->id,
                'name'          => $a->name,
                'parent_id'     => $a->parent_id,
                'product'       => self::dataFactories($a->product),
            ]);
        };

        return $dataku;
    }

    public function dataFactories($data){
        $dataku = [];
        foreach ($data as $a) {
            array_push($dataku,[
                'id'            => $a->id,
                'name'          => $a->name,
                'harga'         => $a->harga,
                'tanggal'       => $a->created_at,
            ]);
        };

        return $dataku;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($user_id, Request $request)
    {
        $data = $this->model->create([
            'parent_id'     => $request['parent_id'],
            'name'          => $request['name'],
        ]);

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Data berhasil ditambahkan...!',
            'data'          => $data,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id, $category_id)
    {
        $data = $this->model->where('id', $category_id)->get();

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Detail data',
            'data'          => $data,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($user_id, Request $request, $category_id)
    {
        $data = $this->model->where('id', $category_id);
        $data->update([
            'parent_id'     => $request['parent_id'],
            'name'          => $request['name'],
        ]);

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Data berhasil diupdate...!',
            'user_id'           => $user_id,
            'name'           => $request['name'],
            'parent_id'           => $request['parent_id'],
            'category_id'           => $category_id,
            'data'          => $data,
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id, $category_id)
    {
        $data = $this->model->where('id', $category_id)->delete();

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Data berhasil dihapus...!',
        ], 200);
    }
}
