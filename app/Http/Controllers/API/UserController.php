<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\User;

class UserController extends Controller
{

    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->model->all();
        
        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Berhasil...!',
            'data'          => $data,
        ], 200); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->model->create([
            'name'      => $request['name'],
            'username'  => $request['username'],
            'email'     => $request['email'],
            'password'  => Hash::make($request['password']),
            'role'      => 1,
        ]);

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Berhasil...!',
            'data'          => $data,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        $data = $this->model->where('id', $user_id)->get();

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Berhasil...!',
            'data'          => $data,
        ], 200);
    }

    public function admin($user_id, $id)
    {
        $data = $this->model->where('id', $id)->get();

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Berhasil...!',
            'data'          => $data,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id)
    {
        $data = $this->model->where('user_id', $user_id)->update([
            'name'      => $request['name'],
            'username'  => $request['username'],
            'email'     => $request['email'],
            'password'  => Hash::make($request['password']),
            'role'      => 1,
        ]);
        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Berhasil...!',
            'data'          => $data,
        ], 200);
    }
}
