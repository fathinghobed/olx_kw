<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Bank;
use App\Http\Controllers\API\UploadkuHandler;

class BankController extends Controller
{

    protected $model;
    protected $upload;

    public function __construct(Bank $model, UploadkuHandler $upload)
    {
        $this->model = $model;
        $this->upload = $upload;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->model->all();

        return response()->json([
            'status_code'   => 200,
            'msg'           => 'success',
            'data'          => $data,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($user_id, Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'name' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg|max:1000'
        ]);

        $image = $this->upload->upload($request['image']);

        $data = $this->model->create([
            'code' => $request['code'],
            'name' => $request['name'],
            'image' => $image,
        ]);

        return response()->json([
            'status_code'   => 200,
            'msg'           => 'success',
            'data'          => $data,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id, $bank_id)
    {
        $data = $this->model->where('id', $bank_id)->get();

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Detail data',
            'data'          => $data,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($user_id, Request $request, $bank_id)
    {
        $this->validate($request, [
            'update_image' => 'image|mimes:jpg,png,jpeg|max:1000'
        ]);

        if ($request['update_image'] != NULL) {
            $data = $this->model->find($bank_id);
            $this->upload->deletegambar($data->image);
            $image = $this->upload->upload($request['update_image']);

            $data = $this->model->where('id', $bank_id)->update([
                'name' => $request['update_name'],
                'image' => $image,
            ]);
        }else{
            $data = $this->model->where('id', $bank_id)->update([
                'name' => $request['update_name'],
            ]);
        }

        return response()->json([
            'status_code'   => '200',
            'msg'           => 'Detail berhasil diupdate...!',
            'data'          => $data,
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id, $bank_id)
    {
        $data = $this->model->where('id', $bank_id)->first();
        $hapus = $this->upload->deletegambar($data->image);
        if ($hapus = true) {
            $data = $this->model->where('id', $bank_id)->delete();
            return response()->json([
                'status_code'   => '200',
                'msg'           => 'Detail berhasil dihapus...!',
            ], 200);
        }else{
            return response()->json([
                'status_code'   => '400',
                'msg'           => 'Error Anjing',
            ], 200);
        }
    }
}
